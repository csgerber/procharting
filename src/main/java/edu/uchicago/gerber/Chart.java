/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.uchicago.gerber;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.text.Document;

/**
 *
 * @author ag
 */
public class Chart extends javax.swing.JFrame {

   // private JPopupMenu popMenu2;
    private Properties props;
    
    /**
     * Creates new form Chart
     */
    public Chart() {
        initComponents();
        populatePopup();
        
        //popMenu2 = new JPopupMenu();
    }
    
    private void populatePopup(){
        
         props = new Properties();
        
        String strUserDir = System.getProperties().getProperty("user.dir");
        String strConifFilePath = strUserDir + "/config.properties";
        File file = new File(strConifFilePath); 
        try 
            (
              FileInputStream input = new FileInputStream(file);
            )
        {

            props.load(input);
//            for (String key : props.stringPropertyNames()) {
//                popMenu.add(new JMenuItem(key + " | " + props.getProperty(key)));
//            }
        } catch (Exception e) {

            System.err.println(e.getMessage());
        } 
    }
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        popMenu = new javax.swing.JPopupMenu();
        jScrollPane1 = new javax.swing.JScrollPane();
        txaArea = new javax.swing.JTextArea();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        mniExit = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        mnuAbout = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        txaArea.setColumns(20);
        txaArea.setRows(5);
        txaArea.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txaAreaKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(txaArea);

        jMenu1.setText("File");

        mniExit.setText("Exit");
        mniExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniExitActionPerformed(evt);
            }
        });
        jMenu1.add(mniExit);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        mnuAbout.setText("About");
        jMenuBar1.add(mnuAbout);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 547, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 448, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mniExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniExitActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_mniExitActionPerformed

    private void txaAreaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txaAreaKeyPressed
        // TODO add your handling code here:
            
        //System.out.println("inside event handler");
         if (evt.isControlDown()) {
            if (evt.getKeyCode() == KeyEvent.VK_SPACE) {
               // System.out.println("hello there");
                Point point  = txaArea.getCaret().getMagicCaretPosition();
                popMenu.removeAll();
                
                //get the last word in the text area
                String strLast = getLastWord();
                
                //for each element in props
                JMenuItem menuItem;
                   for (String key : props.stringPropertyNames()) {
                       if (key.contains(strLast)){
                           
                               menuItem = new JMenuItem(key + " | " + props.getProperty(key));
                              menuItem.addActionListener(new MenuActionListener());
                                  popMenu.add(menuItem);
                           
                            //popMenu.add(new JMenuItem(key + " | " + props.getProperty(key)));
                       }
                       
                }
             
      
                
                if (point != null){
                     popMenu.show(txaArea, point.x, point.y);  
                   
                } else {
                      popMenu.show(txaArea, 0, 0);  
                }
                 
                
            }
                
         }
    }//GEN-LAST:event_txaAreaKeyPressed

    
     class MenuActionListener implements ActionListener {
       @Override
        public void actionPerformed(ActionEvent e) {
           //System.out.println("Selected: " + e.getActionCommand());
           
           int nCount = getLastWordCount();
           Document doc = txaArea.getDocument();
           
           try {
           
                doc.remove(doc.getLength() - nCount, nCount );
                doc.insertString(doc.getLength(), getValueFromPiped(e.getActionCommand()), null);
                
           } catch(Exception ex){
               
           }


        }
   }
    
      private String getLastWord(){
       String strText = txaArea.getText();
       return strText.substring(strText.lastIndexOf(" ") + 1);

    }
    private int getLastWordCount(){
      
       String strLast = getLastWord();
       return strLast.length();

    }
    
        private String getValueFromPiped(String str){
           return str.substring(str.lastIndexOf("|") + 2);
     }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Chart.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Chart.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Chart.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Chart.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Chart().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JMenuItem mniExit;
    private javax.swing.JMenu mnuAbout;
    private javax.swing.JPopupMenu popMenu;
    private javax.swing.JTextArea txaArea;
    // End of variables declaration//GEN-END:variables
}
